#include<stdio.h>
struct Fraction
{
    int Nr;
    int Dr;
};
typedef struct Fraction fraction;
struct n_fraction
{
    int n;
    fraction f[10];
};
typedef struct n_fraction n_fraction;
int get_n()
{
    int n;
    scanf("%d",&n);
    return n;
}
n_fraction get_one_input()
{
    n_fraction x;
    int n;
    scanf("%d",&x.n);
    for(int i=0;i<x.n;i++)
    {
        x.f[i].Nr=1;
        scanf("%d",&x.f[i].Dr);
    }
    return x;
}
void get_n_input(int cases,n_fraction arr[])
{
    for(int i=0;i<cases;i++)
    {
        arr[i]=get_one_input();
    }
}
fraction sum(fraction a,fraction b)
{
    fraction res;
    if(a.Dr==b.Dr)
    {
        res.Dr=a.Dr;
        res.Nr=a.Nr+b.Nr;
    }
    else
    {
        res.Dr=a.Dr*b.Dr;
        res.Nr=(a.Nr*b.Dr)+(b.Nr*a.Dr);
    }
    return res;
}
int find_gcd(int a,int b)
{
    int temp;
    while(a!=0)
    {
        temp=a;
        a=b%a;
        b=temp;
    }
    return b;
}

fraction compute_sum(fraction res)
{
    int gcd=find_gcd(res.Nr,res.Dr);
    res.Nr=res.Nr/gcd;
    res.Dr=res.Dr/gcd;
    return res;
}
fraction compute_one_input(n_fraction arr)
{
    fraction res;
    res.Nr=0;
    res.Dr=1;
    for(int i=0;i<arr.n;i++)
    {
        res=sum(res,arr.f[i]);
    }
    res=compute_sum(res);
    return res;
}
void compute_n_input(int cases,n_fraction arr[],fraction res[])
{
    for(int i=0;i<cases;i++)
    {
       res[i]=compute_one_input(arr[i]);
    }
}


void print_one_output(n_fraction arr,fraction res)
{
    for(int i=0;i<arr.n;i++)
    {
        printf("%d/%d ",arr.f[i].Nr,arr.f[i].Dr);
        if(i!=(arr.n-1))
            printf("+ ");
    }
    printf(" = %d/%d\n",res.Nr,res.Dr);
}
void print_n_output(n_fraction arr[],int cases,fraction res[])
{
    for(int i=0;i<cases;i++)
    {
        print_one_output(arr[i],res[i]);
    }
}

int main()
{
    int cases;
    n_fraction arr[10];
    fraction res[10];
    cases=get_n();
    get_n_input(cases,arr);
    compute_n_input(cases,arr,res);
    print_n_output(arr,cases,res);
    return 0;
}
