#include<stdio.h>
struct stud_info
{
    char stud_name[100];
    int marks[10];
    float total_score;
    char *grade_stud;
};
struct gradebook
{
    char name[20];
    int stud_no,w;
    float weights[10];
    struct stud_info ss[10];
};
typedef struct gradebook grade;

int get_n()
{
    int n;
    scanf("%d",&n);
    return n;
}
grade input_one_student()
{
    grade record;
    scanf("%s",record.name);
    scanf("%d%d",&record.stud_no,&record.w);
    for(int i=0;i<record.w;i++)
    {
        scanf("%f",&record.weights[i]);
    }
    for(int i=0;i<record.stud_no;i++)
    {
        scanf("%s",record.ss[i].stud_name);
        for(int j=0;j<record.w;j++)
        {
            scanf("%d",&record.ss[i].marks[j]);
        }
    }
    return record;
}
void input_n_student(int n,grade record[])
{
    for(int i=0;i<n;i++)
    {
        record[i]=input_one_student();
    }
}
grade compute_one_student(grade record)
{
    float total=0.0,sum=0.0,final_score;;
    for(int i=0;i<record.w;i++)
    {
        sum=sum+record.weights[i];
    }
    for(int k=0;k<record.stud_no;k++)
    {
        for(int i=0;i<record.w;i++)
        {
            total=total+(record.weights[i]*record.ss[k].marks[i]);
        }
        final_score=total/sum;
        record.ss[k].total_score=final_score;
        if(final_score>=0 && final_score<60)
        {
           record.ss[k].grade_stud="F";
        }
        else if(final_score>=60 && final_score<70)
        {
           record.ss[k].grade_stud="D";
        }
        else if(final_score>=70 && final_score<80)
        {
           record.ss[k].grade_stud="C";
        }
        else if(final_score>=80 && final_score<90)
        {
           record.ss[k].grade_stud="B";
        }
        else
        {
           record.ss[k].grade_stud="A";
        }
        total=0.0;
    }
    return record;

}
void compute_n_student(int n,grade record[],grade res[])
{
    for(int i=0;i<n;i++)
    {
    res[i]=compute_one_student(record[i]);
    }
}
void print_grade_one_student(grade res)
{
    printf("%s\n",res.name);
    for(int i=0;i<res.stud_no;i++)
    {
        printf("%s   %.2f  %c\n",res.ss[i].stud_name,res.ss[i].total_score,*res.ss[i].grade_stud);
    }
    printf("\n");
}
void print_grade_n_student(int n,grade res[])
{
    printf("\n");
    for(int i=0;i<n;i++)
    {
        print_grade_one_student(res[i]);
    }
}

int main()
{
    int n;
    n=get_n();
    grade record[3],res[3];
    input_n_student(n,record);
    compute_n_student(n,record,res);
    print_grade_n_student(n,res);
 }
